const { random, cos, sin, sqrt, atan2, PI } = Math
const TWO_PI = PI * 2

class Vector {
  constructor (x, y) {
    this.x = x
    this.y = y
  }

  add (v) {
    return new Vector(this.x + v.x, this.y + v.y)
  }

  sub (v) {
    return new Vector(this.x - v.x, this.y - v.y)
  }

  mult (n) {
    return new Vector(this.x * n, this.y * n)
  }

  div (n) {
    return new Vector(this.x / n, this.y / n)
  }

  mag () {
    return sqrt(this.x * this.x + this.y * this.y)
  }

  normalize () {
    return this.div(this.mag())
  }

  limit (max) {
    if (this.mag() > max) {
      this.normalize()
      this.mult(max)
    }
  }

  setMag (n) {
    this.normalize()
    this.mult(n)
  }

  heading () {
    return atan2(this.y, this.x)
  }

  rotate (a) {
    const newHeading = this.heading() + a
    const mag = this.mag()
    this.x = cos(newHeading) * mag
    this.y = sin(newHeading) * mag
  }

  copy () {
    return new Vector(this.x, this.y)
  }

  static fromAngle (a) {
    return new Vector(cos(a), sin(a))
  }

  static random2D () {
    return Vector.fromAngle(random(TWO_PI))
  }

  static random3D () {
    const angle = random(TWO_PI)
    const vz = random(-1, 1)
    const vx = sqrt(1 - vz * vz) * cos(angle)
    const vy = sqrt(1 - vz * vz) * sin(angle)
    return new Vector(vx, vy, vz)
  }

  static lerp (v1, v2, amt) {
    return new Vector(
      (1 - amt) * v1.x + amt * v2.x,
      (1 - amt) * v1.y + amt * v2.y
    )
  }

  static add (v1, v2) {
    return v1.add(v2)
  }

  static sub (v1, v2) {
    return v1.sub(v2)
  }

  static mult (v, n) {
    return v.mult(n)
  }

  static div (v, n) {
    return v.div(n)
  }

  static dist (v1, v2) {
    return v1.sub(v2).mag()
  }

  static dot (v1, v2) {
    return v1.x * v2.x + v1.y * v2.y
  }
}

// Create a canvas filling the screen and get array of points with a curve of mouse clicks

const canvas = document.createElement('canvas')
const scale = window.devicePixelRatio
const ctx = canvas.getContext('2d')
ctx.scale(scale, scale)
canvas.width = window.innerWidth * scale
canvas.height = window.innerHeight * scale
let points = []
let mouseDown
const centerpoint = new Vector(canvas.width / 2, canvas.height / 2)
let time = 0
let drawing = true

window.addEventListener('load', () => {
  document.body.append(canvas)
  window.addEventListener('resize', () => {
    canvas.width = window.innerWidth * scale
    canvas.height = window.innerHeight * scale
  })
  window.addEventListener('mousedown', moveStart)
  window.addEventListener('touchstart', moveStart)
  window.addEventListener('mouseup', moveEnd)
  window.addEventListener('touchend', moveEnd)
  window.addEventListener('mousemove', move)
  window.addEventListener('touchmove', move)
  clearCanvas()
  animate()
})

function moveStart ({ clientX, touches, clientY }) {
  clearCanvas(0.5)
  const { clientX: x, clientY: y } = (touches && touches[0]) || {}
  mouseDown = true
  points = []
  points.push(new Vector(clientX || x, clientY || y))
}

function moveEnd () {
  mouseDown = false
  drawPoints(reorientPoints(points))
}

function move ({ clientX, touches, clientY }) {
  const { clientX: x, clientY: y } = (touches && touches[0]) || {
    clientX,
    clientY
  }
  if (mouseDown) {
    // update the last point if the previous points are proceeding in the same direction
    const lastPoint = points[points.length - 1]
    const secondLastPoint = points[points.length - 2]
    if (!lastPoint || !secondLastPoint) {
      return points.push(new Vector(x, y))
    }
    const lastVector = new Vector(
      lastPoint.x - secondLastPoint.x,
      lastPoint.y - secondLastPoint.y
    )
    const newVector = new Vector(x - lastPoint.x, y - lastPoint.y)
    if (Vector.dot(lastVector, newVector) < 0) {
      points.push(new Vector(x, y))
    } else {
      lastPoint.x = x
      lastPoint.y = y
    }
    ctx.beginPath()
    ctx.strokeStyle = 'white'
    ctx.lineWidth = 2
    ctx.moveTo(lastPoint.x, lastPoint.y)
    ctx.lineTo(x, y)
    ctx.stroke()
  }
  return false
}

function drawPoints (points) {
  ctx.beginPath()
  ctx.strokeStyle = 'white'
  ctx.lineWidth = 2
  for (let i = 0; i < points.length; i++) {
    if (i > 1) {
      const x = (points[i][0] + points[i - 1][0]) / 2
      const y = (points[i][1] + points[i - 1][1]) / 2
      ctx.lineTo(x, y)
    }
    const { x, y } = points[i]
    if (i === 0) ctx.moveTo(x, y)
    else ctx.lineTo(x, y)
  }
  ctx.stroke()
}

function clearCanvas (alpha) {
  ctx.beginPath()
  if (alpha) ctx.fillStyle = `rgba(0, 0, 0, ${alpha})`
  else ctx.fillStyle = 'black'
  ctx.fillRect(0, 0, canvas.width, canvas.height)
  ctx.fillStyle = 'white'
}

// reorient all points on the curve to be relative to the center of the canvas
function reorientPoints (points) {
  const center = findCenter(points)
  return points.map(v => v.sub(center))
}

// find the center weight point of the all the points on the curve
function findCenter (points) {
  let x = 0
  let y = 0
  for (let i = 0; i < points.length; i++) {
    const { x: xi, y: yi } = points[i]
    x += xi
    y += yi
  }
  x /= points.length
  y /= points.length
  return new Vector(x, y)
}

// create a function to find the fourier coefficients of a points on a curve based on the centerpoint
function fourierTransform (pointVectors) {
  const fourierCoefficients = []
  const N = pointVectors.length
  for (let k = 0; k < N; k++) {
    let re = 0
    let im = 0
    for (let n = 0; n < N; n++) {
      const { x, y } = pointVectors[n]
      const phi = (TWO_PI * k * n) / N
      re += x * cos(phi) + y * sin(phi)
      im -= x * sin(phi) - y * cos(phi)
    }
    re = re / N
    im = im / N
    const freq = k
    const amp = sqrt(re * re + im * im)
    const phase = atan2(im, re)

    fourierCoefficients.push({ re, im, freq, amp, phase })
  }
  return fourierCoefficients
}

// use time and the Fourier coefficients to draw the epicycles spinning out from one another so that the last point will be animated along the points on the line.
function drawEpicycles (fourierCoefficients) {
  const circleColor = 'rgba(255, 180, 0, 1)'
  const lineColor = 'rgba(255, 255, 255, 1)'
  const pointColor = 'rgba(0, 0, 255, 1)'
  const pointSize = 8
  const lineSize = 1
  const circleSize = 1
  const center = findCenter(points)
  // draw the centerpoint
  ctx.beginPath()
  ctx.fillStyle = pointColor
  ctx.arc(center.x, center.y, pointSize, 0, TWO_PI)
  ctx.fill()

  // draw the epicycles
  let x = 0
  let y = 0
  for (let i = 0; i < fourierCoefficients.length; i++) {
    const { freq, amp, phase } = fourierCoefficients[i]
    const prevx = x + center.x
    const prevy = y + center.y
    const radius = amp
    const angle = freq * (time + phase)
    x += radius * cos(angle)
    y += radius * sin(angle)
    ctx.beginPath()
    ctx.strokeStyle = circleColor
    ctx.lineWidth = circleSize
    ctx.arc(prevx, prevy, radius, 0, TWO_PI)
    ctx.stroke()
    ctx.beginPath()
    ctx.strokeStyle = lineColor
    ctx.lineWidth = lineSize
    ctx.moveTo(prevx, prevy)
    ctx.lineTo(x + center.x, y + center.y)
    ctx.stroke()
  }

  // draw the last point
  ctx.beginPath()
  ctx.fillStyle = pointColor
  ctx.arc(x + center.x, center.y + y, pointSize, 0, TWO_PI)
  ctx.fill()

  drawPoints(points.map(v => v.add(centerpoint)))

  console.log('drew', fourierCoefficients.length, 'epicycles')
}

// Animate the epicycles by updating the phase angles of each circle over time.
function animate () {
  if (drawing) {
    time += 0.01
    const centeredPoints = reorientPoints(points)
    const fourierCoefficients = fourierTransform(centeredPoints)
    clearCanvas(0.17)
    drawEpicycles(fourierCoefficients)
    drawPoints(points)

    window.requestAnimationFrame(animate)
  } else {
    clearCanvas()
    drawPoints()
  }
}

// start animating when pressing enter
document.addEventListener('keydown', function (e) {
  if (e.code === 'Space') {
    if (drawing) {
      clearCanvas(0.8)
      drawPoints()
      return (drawing = false)
    }
    drawing = true
    animate()
  }
})
